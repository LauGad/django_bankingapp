import os
from twilio.rest import Client


# Your Account Sid and Auth Token from twilio.com/console
# and set the environment variables. See http://twil.io/secure
account_sid = os.environ['TWILIO_ACCOUNT_SID']
auth_token = os.environ['TWILIO_AUTH_TOKEN']
client = Client(account_sid, auth_token)
  
def send_sms_loan_approved(msg_dict):
  message = client.messages.create(
                       body=f"Hi! Your ${msg_dict['balance']} loan has been approved",
                       from_='+18572147872',
                       to=f"{msg_dict['phone']}"
                   )
  
  print(message.sid)