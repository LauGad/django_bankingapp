from rest_framework import serializers

from .models import Customer, BankAccount, Ledger, BankEmployee, Loan
        
class BankAccountSerializer(serializers.ModelSerializer):
    class Meta:
        model = BankAccount
        fields = ['id', 'balance', 'open_date', 'customer']
        
