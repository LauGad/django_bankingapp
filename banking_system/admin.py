from django.contrib import admin
from . import models

admin.site.register(models.Customer)
admin.site.register(models.BankAccount)
admin.site.register(models.Ledger)
admin.site.register(models.BankEmployee)
admin.site.register(models.Loan)
admin.site.register(models.Bank)