from django.db import models
from django.contrib.auth.models import User
from django.contrib.auth.models import AbstractUser
from django.db.models.query import QuerySet
from decimal import Decimal
from django.db import transaction
from uuid import uuid4


# Create your models here.

#class User(AbstractUser):
#      EMPLOYEE = 1
#     CUSTOMER = 2
#      
#      ROLE_CHOICES = (
#          (EMPLOYEE, 'Employee'),
#          (CUSTOMER, 'Customer'),
#      )
#      role = models.PositiveSmallIntegerField(choices=ROLE_CHOICES, blank=True, null=True)

#class User(AbstractUser):
 # name = models.CharField(max_length=50, null=True)
 # phone = models.CharField(max_length=50, null=True)
 # address = models.CharField(max_length=50, null=True)
 # dob = models.DateField(null=True)

class Customer(models.Model):
    RANK = (
      ('Basic', 'Basic'), 
      ('Silver', 'Silver'), 
      ('Gold', 'Gold'), 
    )

    user = models.OneToOneField(User, on_delete=models.CASCADE, null=True)
    name = models.CharField(max_length=50, null=True)
    phone = models.CharField(max_length=50, null=True)
    address = models.CharField(max_length=50, null=True)
    dob = models.DateField(null=True)
    rank = models.CharField(max_length=50, null=True, choices=RANK)

    @property
    def accounts(self) -> QuerySet:
        return BankAccount.objects.filter(customer = self)

    @property
    def loans(self) -> QuerySet:
        return Loan.objects.filter(customer = self)
    
    def __str__(self):
        if self.name==None:
            return "ERROR-CUSTOMER NAME IS NULL"
        return self.name

class BankAccount(models.Model):
    #balance = models.FloatField(default=0)
    open_date = models.DateTimeField(auto_now_add=True, blank=True)
    customer = models.ForeignKey(Customer, on_delete=models.CASCADE, blank=True, null=True)
    is_bank = models.BooleanField(default=False)
    
    @property
    def transactions(self) -> QuerySet:
        return Ledger.objects.filter(account=self.id).exclude(status='Pending')
        
    @property
    def balance(self) -> Decimal:
        return Ledger.objects.filter(account=self.id).exclude(status='Pending').aggregate(
            models.Sum('amount'))['amount__sum'] or Decimal('0')

    def __str__(self):
        if self.customer is None:
            return f"Bank - {self.id}  - {self.balance} - {self.open_date}"
        return f"{self.customer}  - {self.id}  - {self.balance} - {self.open_date}"

class BankEmployee(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=50)
    phone = models.CharField(max_length=20)
    address = models.CharField(max_length=50)
    dob = models.DateField()
    start_date = models.DateField()
    salary = models.FloatField()

    def __str__(self):
        return self.name

class Loan(models.Model):
    STATUS = (
      ('Pending', 'Pending'),
      ('Approved', 'Approved'),
    )

    amount = models.FloatField(default=0)
    recipient_account = models.ForeignKey(BankAccount, on_delete=models.DO_NOTHING, default=1)
    request_date = models.DateTimeField(auto_now_add=True, blank=True)
    start_date = models.DateField(blank=True, null=True)
    term = models.IntegerField()
    rate = models.FloatField(blank=True, null=True)
    customer = models.ForeignKey(Customer, on_delete=models.DO_NOTHING)
    approver = models.ForeignKey(BankEmployee, on_delete=models.DO_NOTHING, blank=True, null=True)
    status = models.CharField(max_length=50, null=True, choices=STATUS)

    @property
    def initial_transfer(self):
        return Ledger.objects.get(loan_id = self.id, account = self.recipient_account.id)

    @property
    def payments(self) -> QuerySet:
        return Ledger.objects.filter(is_payment = True, account = self.id)

    @property
    def balance(self) -> Decimal:
        payments = self.payments
        amount = self.initial_transfer.amount
        for p in payments:
            amount -= p.amount
        return amount

    def __str__(self):
        return f"{self.customer}  - {self.amount} - {self.status}"

class Ledger(models.Model):
    STATUS = (
      ('Pending', 'Pending'),
      ('Successful', 'Successful'),
    )
    transaction_id = models.CharField(max_length=36)
    account = models.CharField(max_length=50, blank=True, null=True)
    source = models.CharField(max_length=50)
    destination = models.CharField(max_length=50)
    amount = models.FloatField()
    date = models.DateTimeField(auto_now_add=True, blank=True)
    status = models.CharField(max_length=50, null=True, choices=STATUS)
    is_loan = models.BooleanField(default=False)
    loan_id = models.ForeignKey(Loan, on_delete=models.CASCADE, blank=True, null=True)
    is_payment = models.BooleanField(default=False)
    is_deposit = models.BooleanField(default=False)

    @classmethod
    def transaction(cls, amount, account_from, account_to, status, loan_id=None, is_loan=False, is_payment=False, is_deposit=False):
        with transaction.atomic():
            transaction_id = str(uuid4())
            cls(account=account_from, source = 'self', destination = account_to, transaction_id=transaction_id, amount=-amount, status=status, 
                is_loan=is_loan, loan_id=loan_id, is_payment=is_payment, is_deposit=is_deposit).save()
            cls(account=account_to, source=account_from, destination='self', transaction_id=transaction_id, amount=amount, status=status, 
                is_loan=is_loan, loan_id=loan_id, is_payment=is_payment, is_deposit=is_deposit).save()
        return transaction_id

    @classmethod
    def outside_transaction(cls, amount, account_from, account_to, status, transaction_id, bank):
        with transaction.atomic():
            if transaction_id is None:
                transaction_id = str(uuid4())
                cls(account=account_from, source='self', destination=account_to+" - "+bank, transaction_id=transaction_id, amount=amount, status=status).save()
            else:
                cls(account=account_to, source=account_from+" - "+bank, destination='self', transaction_id=transaction_id, amount=amount, status=status).save()

            return transaction_id

    def __str__(self):
        if self.is_payment is True:
            return f"Payment:{self.account} - {self.date}  - {self.amount} - source: {self.source} - dest: {self.destination}"
        elif self.is_loan is True:
            return f"Loan:{self.account} - {self.date}  - {self.amount} - source: {self.source} - dest: {self.destination}"
        elif self.is_deposit is True:
            return f"Deposit:{self.account} - {self.date}  - {self.amount} - source: Deposit - dest: {self.destination}"
        else:
            return f"Transfer {self.transaction_id}: {self.account} - {self.date}  - {self.amount} - source: {self.source} - dest: {self.destination}"


class Bank(models.Model):
    bank_id = models.IntegerField()
    name = models.CharField(max_length=50)
    address = models.CharField(max_length=50, blank=True, null=True)
    shared_secret = models.CharField(max_length=50)
    ipaddress = models.CharField(max_length=50)
    isthis = models.BooleanField(default=False)

    def __str__(self):
        return f"{self.name} - {self.ipaddress}"
