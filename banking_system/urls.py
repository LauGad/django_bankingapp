from django.urls import path

from . import views

app_name = 'banking_system'

urlpatterns = [
    path('', views.index, name='index'),
    path('account/<str:account_id>/', views.account_info, name='account_info'),
    path('new_bank_account/<str:customer_id>/', views.new_bank_account, name='new_bank_account'),
    path('loan/<str:loan_id>/', views.loan_info, name='loan_info'),
    path('payment/<str:loan_id>/', views.payment_request, name='payment_request'),
    path('payment_conf/', views.payment, name='payment'),
    path('transfer/<str:account_id>/', views.transfer_request, name='transfer_request'),
    path('transfer_attempt/', views.transfer, name='transfer'),
    path('transfer_attempt/', views.transfer, name='transfer_attempt'),

    path('transfer_from_bank/', views.transfer_from_bank, name="transfer_from_bank"),

    path('new_loan/<user_id>/', views.new_loan, name='new_loan'),    
    path('loan_request_sent/', views.loan_request_sent, name='loan_request_sent'),
    path('customer_overview/<str:customer_id>/', views.customer_overview, name='customer_overview'),
    path('new_user/', views.new_user, name='new_user'),
    path('new_customer/', views.new_customer, name='new_customer'),
    path('save_new_customer/', views.save_new_customer, name='save_new_customer'),
    path('loan_approval/<str:loan_id>/', views.loan_approval, name='loan_approval'),
    path('add_funds/<str:account_id>/', views.add_funds, name='add_funds'),
    path('add_funds_complete/', views.add_funds_complete, name='add_funds_complete'),
    #rest
    path('get_bankaccount/<str:account_id>/', views.get_bankaccount, name='get_bankaccount'),
]