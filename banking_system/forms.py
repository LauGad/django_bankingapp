from django.forms import ModelForm
from .models import *
from django import forms

class RankForm(ModelForm):
  class Meta:
    model = Customer
    fields = ['rank']
    
class CustomerForm(ModelForm):
  class Meta:
    model = Customer
    exclude = ['user']
    widgets = {
        'dob': forms.DateInput(format=('%m/%d/%Y'), attrs={ 'placeholder':'Select a date', 'type':'date'}),
    }