from django.shortcuts import render, reverse, redirect
from django.http import HttpResponse
from .models import *
from django.contrib.auth.models import Group
from django.contrib.auth.models import User
import datetime
from django.http import HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth.decorators import permission_required
from django.db import transaction
from .forms import *
from .utils import send_sms_loan_approved
from cryptography.fernet import Fernet
import django_rq
import random
import requests
from uuid import uuid4

import threading
from asgiref.sync import async_to_sync, sync_to_async
from multiprocessing.dummy import Pool

from datetime import date
import pytz

import time

#rest

from django.http import JsonResponse
import io
from rest_framework.parsers import JSONParser
from .serializers import BankAccountSerializer
import json

def get_bankaccount(request, account_id):
    """
    List all bank accounts
    """
    if request.method == 'GET':
        bankaccount = BankAccount.objects.get(id=account_id)
        serializer = BankAccountSerializer(bankaccount)
        return JsonResponse(serializer.data, safe=False)

def bankaccount_exists(request):
    return BankAccount.objects.get(id=request.post['accountid']).exists()


#end rest

@login_required
def index(request):

    context = {}
    if Customer.objects.filter(user = request.user.id).exists():
        customer = Customer.objects.get(user = request.user.id)
        context = {
          'customer' : customer
        }
        return render(request, 'banking_system/index.html', context)

    elif BankEmployee.objects.filter(user = request.user.id).exists():
        employee = BankEmployee.objects.get(user = request.user.id)
        customers = Customer.objects.all()
        accounts = BankAccount.objects.all()
        loans = Loan.objects.all()
        for c in customers:
            c.acc_total = BankAccount.objects.filter(customer = c).count()
            c.loan_approved_total = Loan.objects.filter(customer = c, status = 'Approved').count()
            c.loan_pending_total = Loan.objects.filter(customer = c, status = 'Pending').count()

        context = {
          'customers' : customers,
          'accounts' : accounts
        }
        return render(request, 'banking_system/employee_portal.html', context)

    else:
        pass

@login_required
@permission_required('banking_system.view_customer')
def customer_overview(request, customer_id):
    customer = Customer.objects.get(id = customer_id)
    if request.method == "POST":
        form = RankForm(request.POST, instance = customer)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('banking_system:index'))

    form = RankForm(instance=customer)
    employee = True
    context = {
        'customer' : customer,
        'employee' : employee,
        'form' : form
    }
    return render(request, 'banking_system/index.html', context)


@login_required
def account_info(request, account_id):
    account = BankAccount.objects.get(id=account_id)

    context = {
      'account' : account
    }

    return render(request, 'banking_system/account_info.html', context)

@login_required
def loan_info(request, loan_id):
    employee = False
    if BankEmployee.objects.filter(user = request.user.id).exists():
        employee = True
    loan = Loan.objects.get(id=loan_id)
    context = {
      'loan' : loan,
      'employee' : employee
    }
    return render(request, 'banking_system/loan_info.html', context)

#add permission for loan approval
@login_required
def loan_approval(request, loan_id):
    if request.method == "GET":
        #update loan status
        loan = Loan.objects.get(id=loan_id)
        loan.status = 'Approved'
        loan.approver = BankEmployee.objects.get(user = request.user.id) 
        loan.start_date = date.today()
        loan.save()

        #record transfer
        bank_master_acc = BankAccount.objects.get(is_bank=True)
        Ledger.transaction(loan.amount, bank_master_acc.id, loan.recipient_account.id, 'Successful', loan, True)

        django_rq.enqueue(send_sms_loan_approved, {
               'balance' : loan.amount,
               'phone' : loan.customer.phone,
            })

        return HttpResponseRedirect(reverse('banking_system:index'))
    return render(request, 'banking_system/loan_approval')

@login_required
def new_bank_account(request, customer_id):
    if request.method == "GET":
        account = BankAccount()
        account.customer = Customer.objects.get(id=customer_id)
        account.save()
    return HttpResponseRedirect(reverse('banking_system:index'))

@login_required
def payment_request(request, loan_id):
    loan = Loan.objects.get(id=loan_id)
    context = {
      'loan' : loan,
    }
    return render(request, 'banking_system/payment.html', context)

@login_required
@permission_required('banking_system.add_payment')
def payment(request):
    context = {}
    if request.method == "POST":
        loan_id = request.POST["pk"]
        amount = request.POST["amount"]
        account_id = request.POST["accounts"]
        loan = Loan.objects.get(id=loan_id)
        account = BankAccount.objects.get(id=account_id)

        if not amount:
            context.update({
              'error' : 'Please enter an amount for the payment.'
            })
        else:
            amount = float(amount)
            if amount <= 0:
                context.update ({
                  'error' : 'Please enter a valid amount for the payment.'
                })
            elif amount > account.balance:
                context.update ({
                  'error' : "You don't have enough funds for this payment. Please try again."
                })
            elif amount > loan.balance:
                context.update ({
                  'error' : 'The amount is larger than the loan balance.'
                })
            elif amount and amount>0 and amount<=account.balance and amount <= loan.balance: 

                #record payment
                Ledger.transaction(amount, account.id, loan_id, 'Successful', None, False, True, False)

                return HttpResponseRedirect(reverse('banking_system:index'))
            else:
                pass
    else:
        pass

    context.update({
      'loan' : loan,
    })
    return render(request, 'banking_system/payment.html', context)

@login_required
def transfer_request(request, account_id):

    account = BankAccount.objects.get(id=account_id)
    banks = Bank.objects.all()
    context = {
      'account' : account,
      'banks' : banks
    }
    return render(request, 'banking_system/transfer.html', context)

@login_required
@permission_required('banking_system.add_transfer')
def transfer(request):
    context = {}
    if request.method == "POST":
        bank_name = request.POST["banks"]
        bank = Bank.objects.get(name=bank_name)
        account_from_id = request.POST["pk_from"]
        account_from = BankAccount.objects.select_for_update().get(id=account_from_id)
        account_to_id = request.POST["pk_to"]
        amount = request.POST["amount"]
        
        pool = Pool()
        
        if not account_to_id:
            print("no account")
            context.update({
              'error2' : 'Please enter the destination account for the transfer'
            })
            
        if not amount:
            print("no amount")

            context.update({
              'error1' : 'Please enter the amount for the transfer'
            })
        
        else: 
            amount = float(amount)

            if account_from.balance<=amount:
              print("no funds")

              context.update({
                'error1' : 'You do not have enough funds to complete this transfer'
              })

            elif amount > 0 and account_to_id and account_from.balance>=amount:
              print("OK")
              
              # internal transfer
              if bank.isthis:
                print("internal")

                # record transfer
                Ledger.transaction(amount, account_from_id, account_to_id, 'Successful')

                return HttpResponseRedirect(reverse('banking_system:index'))

              # external
              else:
                print("external")

                transfer_id = Ledger.outside_transaction(-amount, account_from_id, account_to_id, 'Pending', None, bank.name)
                transfer = Ledger.objects.get(transaction_id=transfer_id)

                #encrypt
                bank_secret = bank.shared_secret
                fernet = Fernet(bank_secret)
                string = f'{transfer_id}/{amount}/{account_from_id}/{account_to_id}/{transfer.date}'
                encrypted_transfer = fernet.encrypt(bytes(string, 'utf-8'))

                url = bank.ipaddress + "/bank/transfer_from_bank/"
                url_ext = bank.ipaddress + "/bank/get_bankaccount/"+account_to_id
                print("step 0")
                thisBank = Bank.objects.get(isthis=True)
                print(thisBank)
                transfer_obj = {
                  'transfer': encrypted_transfer,
                  'bank' : thisBank.ipaddress,
                  'step' : 1
                }
                print(transfer_obj)
                
                #pollr = pool.apply_async(requests.post, args=(url, transfer_obj))
                #print(pollr.get())
                
                preq = requests.get(url_ext)
                if preq.status_code == 200: 
                  pool.apply_async(requests.post, args=(url, transfer_obj))
                else:
                  banks = Bank.objects.all()
                  context.update({
                    'account' : account_from,
                    'banks' : banks,
                    'error1' : 'account does not exist'
                  })
                  
                  return render(request, 'banking_system/transfer.html', context)

                # return HttpResponseRedirect(reverse('banking_system:index'))
                
                print('after pool')
            else:
                pass # parameters were not met to complete the transfer

    banks = Bank.objects.all()
    context.update({
      'account' : account_from,
      'banks' : banks
    })
    
    return render(request, 'banking_system/transfer.html', context)

def transfer_from_bank(request):
    data = request.POST.get('transfer', False)
    step = request.POST.get('step', False)
    bank_ip = request.POST.get('bank', False)

    bank = Bank.objects.get(ipaddress=bank_ip)
    thisBank = Bank.objects.get(isthis=True)

    fernet = Fernet(bank.shared_secret)
    transfer_str = fernet.decrypt(data.encode("utf-8")).decode("utf-8") #add try/catch
    data = transfer_str.split("/")

    pool = Pool()

    #recipient
    if step == "1":
        print("STEP 1")
        transfer_id = data[0] 
        amount = data[1] 
        account_from = data[2] 
        account_to = data[3]
        transfer_date = data[4]#end try/catch and throw exception - report log: transfer failed (add transfer timeout)

        account_
        #record pending transfer
        Ledger.outside_transaction(amount, account_from, account_to, 'Pending', transfer_id, bank.name)

        #validation
        url = bank.ipaddress + "/bank/transfer_from_bank/"
        if (datetime.datetime.strptime(transfer_date, "%Y-%m-%d %H:%M:%S.%f%z") - datetime.datetime.now(pytz.utc)) < datetime.timedelta(minutes=1):
            #encrypt
            string = f'{transfer_id}'
            encrypted_transfer = fernet.encrypt(bytes(string, 'utf-8'))

            transfer_obj = {
              'transfer': encrypted_transfer,
              'bank' : thisBank.ipaddress,
              'step' : 2
            }

        else:
            #failed transaction
            transfer_obj = {
              'error' : 'Failed Transaction'
            }

        pool.apply_async(requests.post, args=(url, transfer_obj))

    #sender
    elif step == "2":
        print("STEP 2")
        transfer = Ledger.objects.get(transaction_id=data[0])
        if transfer.status=='Pending':
            transfer.status = 'Successful'
            transfer.save()

            #encrypt
            string = f'{transfer.transaction_id}'
            encrypted_transfer = fernet.encrypt(bytes(string, 'utf-8'))

            url = bank.ipaddress + "/bank/transfer_from_bank/"
            transfer_obj = {
              'transfer': encrypted_transfer,
              'bank' : thisBank.ipaddress,
              'step' : 3
            }
            try:
                print("sending request to "+ url)
                # requests.post(url, data = transfer_obj)
                pool.apply_async(requests.post, args=(url, transfer_obj))

            except requests.exceptions.ConnectionError:
                print("Connection refused")
        else: 
            pass #don't repeat transfer

    #recipient
    elif step == "3":
        print("STEP 3")
        transfer = Ledger.objects.get(transaction_id=data[0])
        if transfer.status=='Pending':
            transfer.status = 'Successful'
            transfer.save()

            url = bank.ipaddress + "/bank/"
            print(url)
            pool.apply_async(requests.get, args=(url))

        else: 
            pass #don't repeat transfer

  
    
    pool.close()
    return HttpResponseRedirect(reverse('banking_system:index'))


@login_required  
def new_loan(request, user_id):
  
    customer = Customer.objects.get(user = user_id)
    context = {
        'customer' : customer,
    }
    return render(request, 'banking_system/new_loan.html', context)

@login_required  
@permission_required('banking_system.add_loan')
def loan_request_sent(request):
    context = {}
    if request.method == "POST":
        amount = request.POST["amount"]
        term = request.POST["term"]
        account_id = request.POST["accounts"]
        customer = Customer.objects.get(user = request.user.id)

        if not account_id:
            context.update({
                'error' : 'Please choose a recipient account'
              })
        elif not amount or float(amount)<0:
            context.update({
                'error' : 'Please enter a valid amount'
              })

        elif not term:
            context.update({
                'error' : 'Please enter a term'
              })

        else:
            loan = Loan()
            loan.amount = amount
            loan.recipient_account = BankAccount.objects.get(id=account_id)
            loan.term = term
            loan.customer = customer
            loan.status = 'Pending'
            loan.save()

            return HttpResponseRedirect(reverse('banking_system:index'))

    context.update({
      'customer' : customer,
    })
    return render(request, 'banking_system/new_loan.html', context)

@login_required   
@permission_required('auth.add_user')
def new_user(request):
    context = {}
    return render(request, 'banking_system/new_user.html', context)
  
@login_required 
@permission_required('banking_system.add_customer') 
def new_customer(request):
    context = {}
    if request.method == "POST":
        password = request.POST['password']
        confirm_password = request.POST['confirm_password']
        user_name = request.POST['user']
        email = request.POST['email']
        if user_name and email and password:
            if password == confirm_password:
                if User.objects.create_user(user_name, email, password):
                  user = User.objects.get(username = user_name)
                  print(user)
                  print(user.id)
                  form = CustomerForm()
                  context = {
                    'user' : user,
                    'form' : form
                  }
                  #assign group permissions
                  cGroup = Group.objects.get(name='Customers') 
                  cGroup.user_set.add(user)
                  return render(request, 'banking_system/new_customer.html', context)
                else:
                  context = {
                      'error': 'Could not create user account - please try again.'
                  }
            else:
                context = {
                  'error': 'Passwords did not match. Please try again.'
                }
        else:
            context = {
              'error': 'Please enter all the login information'
            }

    return render(request, 'banking_system/new_customer.html', context)


def save_new_customer(request):
    context = {}
    if request.method == "POST":  
        form = CustomerForm(request.POST)
        if form.is_valid():
            customer = form.save()
            customer.user = User.objects.get(id=request.POST['user'])
            customer.save()
            return HttpResponseRedirect(reverse('banking_system:index')) 

    context = {
      'user' : request.POST['user'],
      'form' : form
    }
    return render(request, 'banking_system/new_customer.html', context)

def add_funds(request, account_id):
    account = BankAccount.objects.get(id=account_id)
    context = {
      'account' : account
    }
    return render(request, 'banking_system/add_funds.html', context)

def add_funds_complete(request):
    if request.method == "POST":
        Ledger.transaction(float(request.POST['amount']), 'Deposit', request.POST['pk'], 'Successful', None, False, False, True)

    return HttpResponseRedirect(reverse('banking_system:index'))
