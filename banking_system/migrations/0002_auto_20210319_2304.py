# Generated by Django 3.1.7 on 2021-03-19 23:04

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('banking_system', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='bankemployee',
            name='phone',
            field=models.CharField(max_length=20),
        ),
        migrations.AlterField(
            model_name='customer',
            name='phone',
            field=models.CharField(max_length=20),
        ),
    ]
