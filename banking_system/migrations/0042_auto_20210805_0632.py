# Generated by Django 3.1.7 on 2021-08-05 06:32

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('banking_system', '0041_auto_20210805_0534'),
    ]

    operations = [
        migrations.RenameField(
            model_name='transfer',
            old_name='account_from',
            new_name='account',
        ),
        migrations.RenameField(
            model_name='transfer',
            old_name='transfer_id',
            new_name='transaction_id',
        ),
        migrations.RemoveField(
            model_name='transfer',
            name='account_to',
        ),
        migrations.AlterField(
            model_name='bankaccount',
            name='customer',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='banking_system.customer'),
        ),
    ]
