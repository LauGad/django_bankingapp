from django.contrib import admin
from .models import PasswordResetRequest, Code, LoggedInUser


admin.site.register(PasswordResetRequest)
admin.site.register(Code)
admin.site.register(LoggedInUser)