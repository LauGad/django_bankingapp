from django.shortcuts import render, reverse
from django.contrib.auth import authenticate, login as dj_login, logout as dj_logout
from django.http import HttpResponseRedirect
from .models import PasswordResetRequest
from .forms import CodeForm
from django.contrib.auth.decorators import login_required
from .utils import send_sms_code
from django.contrib.auth.models import User
from django.contrib.auth.models import Group
from banking_system.forms import CustomerForm
from banking_system.models import Customer

def login(request):
   context = {}

   if request.method == "POST":
      user = authenticate(request, username=request.POST['user'], password=request.POST['password'])
      if user:
            request.session['pk'] = user.pk
            # return HttpResponseRedirect(reverse('login_app:verify_user'))
            dj_login(request, user)
            return HttpResponseRedirect(reverse('banking_system:index'))
      else:
            context = {
               'error': 'Bad username or password.'
            }
   return render(request, 'login_app/login.html', context)

def verify_user(request):
  form = CodeForm(request.POST or None)
  pk = request.session.get('pk')
  if pk:
    user = User.objects.get(pk=pk)
    code = user.code
    code_user = f"{user.username}: {user.code}"
    if not request.POST:
      if hasattr(user, 'customer'):
        send_sms_code(code_user, user.customer.phone)
      elif hasattr(user, 'bankemployee'):
        send_sms_code(code_user, user.bankemployee.phone)
      
    if form.is_valid():
      num = form.cleaned_data.get('number')
      
      if str(code) == num:
        #create new code 
        code.save()
        dj_login(request, user)
        return HttpResponseRedirect(reverse('banking_system:index'))
      else:
        return HttpResponseRedirect(reverse('banking_system:login'))

  return render(request, 'login_app/verify.html', {'form': form})
      

def logout(request):
   dj_logout(request)
   return render(request, 'login_app/login.html')


def request_password_reset(request):
 if request.method == "POST":
     post_user = request.POST['username']
     user = None

     if post_user:
         try:
             user = User.objects.get(username=post_user)
         except:
             print(f"Invalid password request: {post_user}")
     else:
         post_user = request.POST['email']
         try:
             user = User.objects.get(email=post_user)
         except:
             print(f"Invalid password request: {post_user}")
     if user:
         prr = PasswordResetRequest()
         prr.user = user
         prr.save()
         print(prr)
         return HttpResponseRedirect(reverse('login_app:password_reset'))

 return render(request, 'login_app/request_password_reset.html')

def password_reset(request):
 if request.method == "POST":
     post_user = request.POST['username']
     password = request.POST['password']
     confirm_password = request.POST['confirm_password']
     token = request.POST['token']

     if password == confirm_password:
         try:
             prr = PasswordResetRequest.objects.get(token=token)
             prr.save()
         except:
             print("Invalid password reset attempt.")
             return render(request, 'login_app/password_reset.html')

         user = prr.user
         user.set_password(password)
         user.save()
         return HttpResponseRedirect(reverse('login_app:login'))

 return render(request, 'login_app/password_reset.html')



def sign_up(request):
   context = {}
   if request.method == "POST":
      password = request.POST['password']
      confirm_password = request.POST['confirm_password']
      user_name = request.POST['user']
      email = request.POST['email']
      if password == confirm_password:
            if User.objects.create_user(user_name, email, password):
               user = User.objects.get(username = user_name)
               customer = Customer(user=user)
               form = CustomerForm(instance=customer)
               context = {
                  'form' : form
               }
               #assign group permissions
               cGroup = Group.objects.get(name='Customers') 
               cGroup.user_set.add(user)
               return render(request, 'banking_system/new_customer.html', context)

               return HttpResponseRedirect(reverse('login_app:login'))
            else:
               context = {
                  'error': 'Could not create user account - please try again.'
               }
      else:
            context = {
               'error': 'Passwords did not match. Please try again.'
            }
   return render(request, 'login_app/sign_up.html', context)


@login_required
def delete_account(request):
   if request.method == "POST":
      if request.POST['confirm_deletion'] == "DELETE":
            user = authenticate(request, username=request.user.username, password=request.POST['password'])
            if user:
               print(f"Deleting user {user}")
               user.delete()
               return HttpResponseRedirect(reverse('login_app:login'))
            else:
               print("fail delete")

   return render(request, 'login_app/delete_account.html')

