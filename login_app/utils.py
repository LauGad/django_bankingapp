import os
from twilio.rest import Client


# Your Account Sid and Auth Token from twilio.com/console
# and set the environment variables. See http://twil.io/secure
account_sid = os.environ['TWILIO_ACCOUNT_SID']
auth_token = os.environ['TWILIO_AUTH_TOKEN']
client = Client(account_sid, auth_token)

def send_sms_code(user_code, phone_number):
  message = client.messages.create(
                       body=f'Hi! Your user and verification code is {user_code}',
                       from_='+18572147872',
                       to=f'{phone_number}'
                   )
  
  print(message.sid)
  
