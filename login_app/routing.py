from django.urls import re_path

from .consumers import WSConsumer

websocket_urlpatterns = [
    re_path(r'ws/bank/', WSConsumer.as_asgi()),
]