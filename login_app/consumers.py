import json
from channels.generic.websocket import AsyncWebsocketConsumer

class WSConsumer(AsyncWebsocketConsumer):
    async def connect(self):
        self.group_name = 'test'

        # Join room group
        await self.channel_layer.group_add(
            self.group_name,
            self.channel_name
        )
        
        await self.accept()

    async def disconnect(self, close_code):
        pass

    # Receive message from WebSocket
    async def receive(self, text_data):
        data_json = json.loads(text_data)
        username = data_json['username']
        token = data_json['token']
        
        # Send message to room group 
        await self.channel_layer.group_send(
            self.group_name, {
            'type' : 'msg',
            'username': username,
            'token': token
        })

    # Receive message from room group     
    async def msg(self, event):
        username = event['username']
        token = event['token']
        await self.send(json.dumps({
            'username' : username,
            'token': token
        }))