from django.contrib.auth.models import User
from django.db import models
from secrets import token_urlsafe
import random
from django.conf import settings

User = settings.AUTH_USER_MODEL

class PasswordResetRequest(models.Model):
   user = models.ForeignKey(User, on_delete=models.CASCADE)
   token = models.CharField(max_length=43, default=token_urlsafe)
   created_timestamp = models.DateTimeField(auto_now_add=True)
   updated_timestamp = models.DateTimeField(auto_now=True)

   def __str__(self):
      return f'{self.user} - {self.created_timestamp} - {self.updated_timestamp} - {self.token}'

class Code(models.Model):
  number = models.CharField(max_length=5, blank=True)
  user = models.OneToOneField(User, on_delete=models.CASCADE)
  
  def __str__(self):
    return str(self.number)
    
  def save(self, *args, **kwargs):
    number_list = [x for x in range(10)]
    code_items = []
    
    for i in range(5):
      num = random.choice(number_list)
      code_items.append(num)
      
    code_string = "".join(str(item) for item in code_items)
    self.number = code_string
    super().save(*args, **kwargs)
    
# Model to store the list of logged in users
class LoggedInUser(models.Model):
    user = models.OneToOneField(User, related_name='logged_in_user', on_delete=models.CASCADE)
    # Session keys are 32 characters long
    session_key = models.CharField(max_length=32, null=True, blank=True)

    def __str__(self):
        return self.user.username